function TextInput (props) {
  const { label } = props
  return (
    <label>
      {label} :
      <input {...props} />
    </label>
  )
}

export default TextInput
