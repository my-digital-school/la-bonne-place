import TextInput from './TextInput'

function LoginForm ({ formData, onChange, onSubmit }) {
  return (
    <form onSubmit={onSubmit}>
      <TextInput
        label='Email'
        type='email'
        name='email'
        value={formData.email}
        onChange={onChange}
      />
      <br />
      <TextInput
        label='Mot de passe'
        type='password'
        name='password'
        value={formData.password}
        onChange={onChange}
      />
      <br />
      <button type='submit'>Se connecter</button>
    </form>
  )
}

export default LoginForm
