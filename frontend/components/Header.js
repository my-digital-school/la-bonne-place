import Link from 'next/link'

function Header () {
  return (
    <header>
      <nav>
        <ul>
          <li>
            <Link href='/'>
              <a>Home</a>
            </Link>
          </li>
          <li>
            <Link href='/auth'>
              <a>Auth</a>
            </Link>
          </li>
        </ul>
      </nav>
    </header>
  )
}

export default Header
