import { useState } from 'react'
import LoginForm from '../components/Forms/LoginForm'
import { login } from '../services/api'

function Auth () {
  const [formData, setFormData] = useState({
    email: 'toto@test.fr',
    password: 'tatatoto'
  })
  const [user, setUser] = useState()

  const handleSubmit = async (event) => {
    event.preventDefault()
    const data = await login(formData)
    setUser(data.user)
  }

  const handleChange = (event) => {
    event.preventDefault()
    setFormData({
      ...formData,
      [event.target.name]: event.target.value
    })
  }

  return (
    <>
      <h1>LOGIN</h1>
      <LoginForm
        formData={formData}
        onChange={handleChange}
        onSubmit={handleSubmit}
      />
      <pre>
        {JSON.stringify(user, null, 2)}
      </pre>
    </>
  )
}

export default Auth
