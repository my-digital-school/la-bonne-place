import axios from 'axios'

export default async function handler (req, res) {
  // Test de la méthode de requête
  if (req.method !== 'POST') {
    return res.status(401).send('method unauthorized')
  }
  // Test des paramètres
  const { email, password } = req.body
  if (!email || !password) return res.status(500).send('Missing data')

  // On authentifie l'utilisateur
  try {
    const credentials = {
      identifier: email,
      password: password
    }
    // Appel à Strapi
    const response = await axios.post(`${process.env.STRAPI_URL}/auth/local`, credentials)
    console.log(response.data)
    return res.send(response.data)
  } catch (error) {
    console.error(error)
    return res.status(500).send(error)
  }
  
 
  return res.status(403).send('Invalid Credentials')
}
